import pygame

gameover = False
size = width, height = 800, 600
screen = None
bg = None

ball_coords_x = 80
ball_coords_y = 60

def handle_events():
    global gameover
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameover = True


def init():
    global screen, bg
    pygame.init()
    bg = (255, 255, 255)
    screen = pygame.display.set_mode(size)

def main():
    ball = pygame.image.load("basketball.png")
    ballrect = ball.get_rect()
    ballrect.x = 100
    ballrect.y = 100
    ball = pygame.transform.scale(ball, (100, 100))
    init()
    vx = 2
    vy = 2
    while not gameover:
        handle_events()
        screen.fill(bg)
        screen.blit(ball, ballrect)
        ballrect.x += vx
        ballrect.y += vy
        if ballrect.x + 100 > width or ballrect.x < 0:
            vx *= -1
        if ballrect.y + 100 > height or ballrect.y < 0:
            vy *= -1
        pygame.display.flip()


main()